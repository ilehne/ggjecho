﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportTrap : MonoBehaviour {

	private GridManager gMScript;

	// Use this for initialization
	void Start () {
		gMScript = GameObject.Find("GridManager").GetComponent<GridManager>();

		Random.Range (0, gMScript.floorTiles.Count);
	}

	public Vector3 getNewPos(){
		return gMScript.getWorldPosFromTile(
			gMScript.floorTiles[
				Random.Range (0, gMScript.floorTiles.Count)]);
	}
		
}
