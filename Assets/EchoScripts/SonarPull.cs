﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SonarPull : MonoBehaviour {

    private GameObject tileUp = null;
    private bool borderUp = false;
    private GameObject tileDown = null;
    private bool borderDown = false;
    private GameObject tileRight = null;
    private bool borderRight = false;
    private GameObject tileLeft = null;
    private bool borderLeft = false;
    private GridManager gridManager;
    private TileSpawner tileSpawner;

    private bool activatedFromUp = false;
    private bool activatedFromDown = false;
    private bool activatedFromRight = false;
    private bool activatedFromLeft = false;

    private int localX;
    private int localY;
    private int mapHeight;
    private int mapWidth;

    private float curBlue;
    private Color curColor;
    private float colorReduction = 0.25f;
    private float peakReduction = 0.02f;
    private float darkThreshold = 0.2f;

    //change this value for one tile for debugging to produce log output
    public bool debugMe = false;

	// Use this for initialization
	void Start () {
        gridManager = GameObject.Find("GridManager").GetComponent<GridManager>();
        tileSpawner = GameObject.Find("LevelLoader").GetComponent<TileSpawner>();
        localX = (int)transform.position.x;
        localY = (int)transform.position.y;
        mapHeight = gridManager.mapHeight;
        mapWidth = gridManager.mapWidth;

        //Check if tile is the border on any side
        if (localY + 1 > mapHeight)
            borderUp = true;
        else
            tileUp = gridManager.getTileFromTileCoord(localX, localY + 1);

        if (localY - 1 < 0)
            borderDown = true;
        else
            tileDown = gridManager.getTileFromTileCoord(localX, localY - 1);

        if (localX + 1 > mapWidth)
            borderRight = true;
        else
            tileRight = gridManager.getTileFromTileCoord(localX + 1, localY);

        if (localX - 1 < 0)
            borderLeft = true;
        else
            tileLeft = gridManager.getTileFromTileCoord(localX - 1, localY);
	}
	
	// Update is called once per frame
	void Update () {
        if (tileSpawner.keyframeActive)
        {
            //reduce current brightness every timeStep
            if (GetComponent<SpriteRenderer>().color.b > darkThreshold)
            {
                //curColor = GetComponent<SpriteRenderer>().color;
                //GetComponent<SpriteRenderer>().color = Color.Lerp(curColor, Color.black, colorReduction);
                if (debugMe) Debug.Log("Before: " + GetComponent<SpriteRenderer>().color.b);
                curBlue = GetComponent<SpriteRenderer>().color.b;
                ApplyColor(curBlue - colorReduction);
                if (debugMe) Debug.Log("After: " + GetComponent<SpriteRenderer>().color.b);
            }
            else //at threshold make tile black and reset directions of last activation
            {
                GetComponent<SpriteRenderer>().color = Color.black;
                activatedFromUp = false;
                activatedFromDown = false;
                activatedFromRight = false;
                activatedFromLeft = false;
            }



            //Use brightness of tile above, if it is a floor tile and brighter
            curBlue = GetComponent<SpriteRenderer>().color.b;
            if (!borderUp && tileUp != null && !activatedFromUp)
            {
				if (tileUp.tag == "floor" || tileUp.tag == "teleport" || tileUp.tag == "wall" || tileUp.tag == "spike")
                {
                    if (tileUp.GetComponent<SpriteRenderer>().color.b > curBlue)
                    {
                        ApplyColor(tileUp.GetComponent<SpriteRenderer>().color.b - peakReduction);
                        activatedFromUp = true;
                        if (debugMe) Debug.Log("I have been activated from above.");
                    }
                }
            }
            else
                if (debugMe) Debug.Log("I ignored a signal from above.");

            //Use brightness of tile beneath, if it is a floor tile and brighter
            curBlue = GetComponent<SpriteRenderer>().color.b;
            if (!borderDown && tileDown != null && !activatedFromDown)
            {
				if (tileDown.tag == "floor" || tileDown.tag == "teleport"	|| tileDown.tag == "wall" || tileDown.tag == "spike")
                {
                    if (tileDown.GetComponent<SpriteRenderer>().color.b > curBlue)
                    {
                        ApplyColor(tileDown.GetComponent<SpriteRenderer>().color.b - peakReduction);
                        activatedFromDown = true;
                        if (debugMe) Debug.Log("I have been activated from below.");
                    }
                }
            }
            else
                if (debugMe) Debug.Log("I ignored a signal from below   .");

            //Use brightness of tile to the right, if it is a floor tile and brighter
            curBlue = GetComponent<SpriteRenderer>().color.b;
            if (!borderRight && tileRight != null && !activatedFromRight)
            {
				if (tileRight.tag == "floor" || tileRight.tag == "teleport" || tileRight.tag == "wall" || tileRight.tag == "spike")
                {
                    if (tileRight.GetComponent<SpriteRenderer>().color.b > curBlue)
                    {
                        ApplyColor(tileRight.GetComponent<SpriteRenderer>().color.b - peakReduction);
                        activatedFromRight = true;
                    }
                }
            }

            //Use brightness of tile to the left, if it is a floor tile and brighter
            curBlue = GetComponent<SpriteRenderer>().color.b;
            if (!borderLeft && tileLeft != null && !activatedFromLeft)
            {
				if (tileLeft.tag == "floor" || tileLeft.tag == "teleport" || tileLeft.tag == "wall" || tileLeft.tag == "spike")
                {
                    if (tileLeft.GetComponent<SpriteRenderer>().color.b > curBlue)
                    {
                        ApplyColor(tileLeft.GetComponent<SpriteRenderer>().color.b - peakReduction);
                        activatedFromLeft = true;
                    }
                }
            }
        }
    }

    void ApplyColor(float newValue)
    {
        GetComponent<SpriteRenderer>().color = new Vector4(newValue, newValue, newValue, 1); ;
    }
}
