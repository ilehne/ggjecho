﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour {

    private GameObject pauseScreen;

	// Use this for initialization
	void Start () {
        pauseScreen = GameObject.Find("PauseScreen");
        pauseScreen.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonUp("Cancel"))
        {
            showOrHidePauseScreen();
        }
    }

    public void showOrHidePauseScreen()
    {
        if (pauseScreen.activeSelf)
        {
            pauseScreen.SetActive(false);
            Time.timeScale = 1.0f;
        }
        else
        {
            pauseScreen.SetActive(true);
            Time.timeScale = 0;
        }
    }
}
