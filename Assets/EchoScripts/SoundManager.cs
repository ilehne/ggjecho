﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	public  AudioSource source;
	public  AudioSource bgSource;
	public  AudioClip[] audioClip;
	/* 0: WIN
	 * 1: DEATH
	 * 2: GOT HURT / LOSE HEART (P1)
	 * 3: TELEPORT
	 * 4: GAIN HEART
	 * 5: POWERUP
	 * 6: SPIKE
	 * 7: SHOUT (P1)
	 * 8: SHOUT (P2)
	 * 9: GOT HURT (P2)
	 */

	// Use this for initialization
	void Start () {
		source = GetComponent<AudioSource>();
		//bgSource.Play ();
	}
	
	public void playSound(int clip){
		source.clip = audioClip [clip];
		source.Play ();
		print ("sound " + clip + " played");
	}
}
