﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TileSpawner : MonoBehaviour {

    [Header("Tile Settings")]
    public int TileSize = 32;
    public GameObject[] Tiles;

    [Header("Level Settings")]
    public TextAsset[] CSVLevelFiles;
    public int LevelChoice = 0;

    private string[,] TileIDGrid;
	public GameObject[,] TileMap;

	int[] mapDimensions;
    private GridManager gridManager;

    public float timer;
    private float timeStep = 0.05f;
    public bool keyframeActive = false;
    public bool afterKeyframeActive = false;


    // Use this for initialization
    void Awake () {
        LevelChoice = LevelChoice < CSVLevelFiles.Length ? LevelChoice : CSVLevelFiles.Length;
        // There can still be null in there, beware. Last 2 indices of each axis are not relevant.
        TileIDGrid = CSVReader.SplitCsvGrid(CSVLevelFiles[LevelChoice].text);

		mapDimensions = new int[2];
		mapDimensions [0] = TileIDGrid.GetLength (0) - 3; //x
		mapDimensions [1] = TileIDGrid.GetLength (1) - 3; //y
		TileMap = new GameObject[mapDimensions [0] + 1, mapDimensions [1] + 1];
        BuildLevel();

        gridManager = GameObject.Find("GridManager").GetComponent<GridManager>();
        gridManager.tiles = TileMap;

        //Start timer for first timeStep
        timer = Time.timeSinceLevelLoad + timeStep;
    }
	
	// Update is called once per frame
	void Update () {
        if (Time.timeSinceLevelLoad > timer)
        {
            if (keyframeActive)
            {
                        keyframeActive = false;
                        timer = Time.timeSinceLevelLoad + timeStep;
                    
            }
            else
            {
                keyframeActive = true;
            }
        }

    }

    void BuildLevel()
    {
        Vector3 TileWorldPosition = Vector3.zero;
		for (int y = mapDimensions [1]; y >= 0; y--)
        {
			for (int x = 0; x <= mapDimensions [0]; x++)
            {
                int TileID = Int32.Parse(TileIDGrid[x, y]);
                TileID = TileID < Tiles.Length ? TileID : Tiles.Length-1;
                int MirroredYCoord = Math.Abs(y - mapDimensions[1]);
                TileMap[x, MirroredYCoord] = Instantiate(Tiles[TileID], TileWorldPosition, Quaternion.identity);
                TileWorldPosition += new Vector3(1, 0.0f, 0.0f);
            }
            TileWorldPosition += new Vector3(-TileIDGrid.GetLength(0)+2, 1, 0.0f);
        }
        
    }
}
