﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GridManager : MonoBehaviour {

	public GameObject[,] tiles;
	public List<GameObject> floorTiles;
	public TileSpawner tileSpawnerScript;
    public int mapHeight;
    public int mapWidth;

	void Start() {

		//tileSpawnerScript = GameObject.Find("LevelLoader").GetComponent<TileSpawner>();
		//tiles = tileSpawnerScript.TileMap;

		floorTiles = new List<GameObject>();
		for (int i = 0; i < tiles.GetLength(0); i++) {
			for (int j = 0; j < tiles.GetLength(1); j++) {
				if(getTileType(tiles[i,j]) == TileType.Floor){
					floorTiles.Add(tiles[i, j]);
				}
			}
		}

	}

	//convert world position to tile coordinates
	public int[] worldPosToTileCoor(Vector3 position){
		int[] tileCoordinates = new int[2];
		tileCoordinates [0] = ((int)Mathf.Floor (position.x));
		tileCoordinates [1] = ((int)Mathf.Floor (position.y));
		return tileCoordinates;
	}

	//get tile corresponding to given tile coordinates
	public GameObject getTileFromTileCoord(int x, int y){
		return tiles [x,y];
	}

	//get tile corresponding to given world position
	public GameObject getTileFromWorldPos(Vector2 position){
		return tiles [worldPosToTileCoor (position) [0], worldPosToTileCoor (position) [1]];
	}

	//get tile type of a given tile
	public TileType getTileType(GameObject tile){
		return tile.GetComponent<TileScript> ().tileType;
	}

	//CAREFUL here 0.5 are added onto the position so its in the middle of the tile
	public Vector3 getWorldPosFromTile(GameObject tile){
		return new Vector3 (tile.transform.position.x+0.5f, tile.transform.position.y+0.5f, 0); 
	}

	public bool isValid(Vector2 tilePos){
		if (getTileType (getTileFromWorldPos (tilePos)) == TileType.Floor) {
			return true;
		} else {
			return false;
		}
	}


}