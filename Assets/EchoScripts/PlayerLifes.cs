﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLifes : MonoBehaviour {

    private static int lifes = 5;
    private GameObject[] hearts;
    public GameObject gameOverScreen;
	public SoundManager sM;

    // Use this for initialization
    void Start () {
		sM = GameObject.Find("SoundManager").GetComponent<SoundManager>();

        //hearts = GameObject.FindGameObjectsWithTag("heart");
        hearts = new GameObject[5];
        hearts[4] = GameObject.Find("Heart0");
        hearts[3] = GameObject.Find("Heart1");
        hearts[2] = GameObject.Find("Heart2");
        hearts[1] = GameObject.Find("Heart3");
        hearts[0] = GameObject.Find("Heart4");
    }

    public static void setLifes(int add)
    {
        if(lifes == 0)
        {
            return;
        }else if (lifes == 5 && add > 0)
        {
            return;
        }
        lifes += add;

    }
	
	// Update is called once per frame
	void Update () {
        updateHearts();
	}

    public void updateHearts()
    {
        switch (lifes)
        {
            case 0:
                hearts[0].SetActive(false);
                hearts[1].SetActive(false);
                hearts[2].SetActive(false);
                hearts[3].SetActive(false);
                hearts[4].SetActive(false);
				sM.playSound (1);
                showGameOverScreen();
                break;
            case 1:
                hearts[0].SetActive(false);
                hearts[1].SetActive(false);
                hearts[2].SetActive(false);
                hearts[3].SetActive(false);
                hearts[4].SetActive(true);
                break;
            case 2:
                hearts[0].SetActive(false);
                hearts[1].SetActive(false);
                hearts[2].SetActive(false);
                hearts[3].SetActive(true);
                hearts[4].SetActive(true);
                break;
            case 3:
                hearts[0].SetActive(false);
                hearts[1].SetActive(false);
                hearts[2].SetActive(true);
                hearts[3].SetActive(true);
                hearts[4].SetActive(true);
                break;
            case 4:
                hearts[0].SetActive(false);
                hearts[1].SetActive(true);
                hearts[2].SetActive(true);
                hearts[3].SetActive(true);
                hearts[4].SetActive(true);
                break;
            case 5:
                hearts[0].SetActive(true);
                hearts[1].SetActive(true);
                hearts[2].SetActive(true);
                hearts[3].SetActive(true);
                hearts[4].SetActive(true);
                break;
            default:
                break;
        }
    }

    void showGameOverScreen()
    {
        Time.timeScale = 0.0f;
        gameOverScreen.SetActive(true);
    }

    public void restartLevel()
    {
        lifes = 5;
        Time.timeScale = 1.0f;
        gameOverScreen.SetActive(false);
        Application.LoadLevel(Application.loadedLevel);
    }

}
