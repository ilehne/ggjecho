﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerTrigger : MonoBehaviour {

	private SoundManager sM;
    public GameObject wonScreen;

    private bool won=false;

	// Use this for initialization
	void Start () {
		sM = GameObject.Find("SoundManager").GetComponent<SoundManager>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonUp("Fire3"))
        {
            won = false;
            Time.timeScale = 1.0f;
            SceneManager.LoadScene("Menu");
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "PlayerCharacter1")
        {

			sM.playSound (0);
            Time.timeScale = 0.0f;
            wonScreen.SetActive(true);
            won = true;
        }

		if (other.gameObject.tag == "spike") {
			Debug.Log ("Trap!");
			if (other.gameObject.GetComponent<SpikeTrap>().isActive) {
				Debug.Log ("Ouch!");
				loseHeart ();
				if (gameObject.name.Contains("1")){
					sM.playSound (2);
				}else{
					sM.playSound (9);
				}
				//TODO: lose lives if tile changes to active while standing on it
				//TODO: continously lose lives if standing on trap
			}
		}

		if (other.gameObject.tag == "teleport") {
			Debug.Log ("teleport activated!");
			transform.parent.position = other.gameObject.GetComponent<TeleportTrap> ().getNewPos ();
			sM.playSound (3);
		}
    }

	void OnTriggerStay2D(Collider2D other){
	
	}

	void loseHeart(){
		PlayerLifes.setLifes (-1);
		sM.playSound (2);
	}



}
