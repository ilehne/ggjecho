﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void loadLevel()
    {
        SceneManager.LoadScene("FINAL_LEVEL");
    }

    public void loadCredits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void loadMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void loadHowTo()
    {
        SceneManager.LoadScene("HowTo");
    }
}
