﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeTrap : MonoBehaviour {

	public bool isActive;
	public Sprite active;
	public Sprite inactive;
	BoxCollider2D boxCol;
	private SoundManager sM;

	void Start () {
		sM = GameObject.Find("SoundManager").GetComponent<SoundManager>();
		InvokeRepeating ("onOff", 1, 3.0f);
		isActive = false;
		boxCol = GetComponent<BoxCollider2D> ();
		boxCol.enabled = false;

	}

	void Update(){
		if (isActive) {
			GetComponent <SpriteRenderer>().sprite = active;
		} else {
			GetComponent <SpriteRenderer>().sprite = inactive;
		}
	}

	void onOff(){
		if (isActive) {
			isActive = false;
			boxCol.enabled = false;
			sM.playSound (6);
		} else {
			isActive = true;
			boxCol.enabled = true;
			sM.playSound (6);
		}
		Debug.Log ("bool is " + isActive);

	}
}
