﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpControl : MonoBehaviour {

    private Light torch;
    private float timer;
    private bool litUp = false;
    public float lightUpLength = 20f;

    //Light variables
    public float rangeLit = 5f;
    public float intensityLit = 5f;
    public float rangeNormal = 2.5f;
    public float intensityNormal = 8f;

	private SoundManager sM;

	// Use this for initialization
	void Start () {
        torch = GetComponentInChildren<Light>();
		sM = GameObject.Find("SoundManager").GetComponent<SoundManager>();
    }
	
	// Update is called once per frame
	void Update () {
		if (litUp && timer <= Time.timeSinceLevelLoad)
        {
            litUp = false;

            torch.range = rangeNormal;
            torch.intensity = intensityNormal;
        }
	}

    void OnTriggerEnter2D (Collider2D col)
    {
        if (col.tag == "PowerUpLight")
        {
            timer = lightUpLength + Time.timeSinceLevelLoad;
            Destroy(col.gameObject);

            torch.range = rangeLit;
            torch.intensity = intensityLit;
            litUp = true;
			sM.playSound (5);
        }

        else if (col.tag == "PowerUpHealth")
        {
            Destroy(col.gameObject);
            PlayerLifes.setLifes(1);
			sM.playSound (4);
        }
    }
}
