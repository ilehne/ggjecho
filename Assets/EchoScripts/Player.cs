﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public float stepSize = 1;
    public float stepDuration = 0.3f;
    private int fps = 30;

    private int player;
    private bool currentlyWalking = false;

    private Animator playerAnim;

	private GridManager gMScript;
    private GameObject currTile;

	private SoundManager sM;


	// Use this for initialization
	void Start () {

		gMScript = GameObject.Find("GridManager").GetComponent<GridManager>();
		sM = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        playerAnim = gameObject.GetComponentInChildren<Animator>();

        if (gameObject.name.Contains("1"))
        {
            player = 1;
        }else
        {
            player = 2;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if(player == 1)
        {
            if (Input.GetAxisRaw("Vertical") > 0)
            {
                StartCoroutine(makeStep("up"));
            }
            else if (Input.GetAxisRaw("Vertical") < 0)
            {
                StartCoroutine(makeStep("down"));
            }
            else if (Input.GetAxisRaw("Horizontal") < 0)
            {
                StartCoroutine(makeStep("left"));
            }
            else if (Input.GetAxisRaw("Horizontal") > 0)
            {
                StartCoroutine(makeStep("right"));
            }
            if (Input.GetButtonUp("Fire1"))
            {
                StartCoroutine(makeSonarWave());
				sM.playSound (7);
            }
        }else if(player == 2)
        {
            if (Input.GetAxisRaw("Vertical2") > 0)
            {
                StartCoroutine(makeStep("up"));
            }
            else if (Input.GetAxisRaw("Vertical2") < 0)
            {
                StartCoroutine(makeStep("down"));
            }
            else if (Input.GetAxisRaw("Horizontal2") < 0)
            {
                StartCoroutine(makeStep("left"));
            }
            else if (Input.GetAxisRaw("Horizontal2") > 0)
            {
                StartCoroutine(makeStep("right"));
            }
            if (Input.GetButtonUp("Fire2"))
            {
                StartCoroutine(makeSonarWave());
				sM.playSound (8);
            }
        }
        
    }

    public IEnumerator makeStep(string direction)
    {
		Vector2 currentPos = gameObject.transform.position;
		Vector2 newPos = calculateNewPos(currentPos, direction);

		if (!currentlyWalking && gMScript.isValid(newPos) )
        {
            currentlyWalking = true;

            if(player == 1)
            {
                playerAnim.SetFloat("walk-y", Input.GetAxisRaw("Vertical"));
                playerAnim.SetFloat("walk-x", Input.GetAxisRaw("Horizontal"));
            }
            if (player == 2)
            {
                playerAnim.SetFloat("walk-y", Input.GetAxisRaw("Vertical2"));
                playerAnim.SetFloat("walk-x", Input.GetAxisRaw("Horizontal2"));
            }

            playerAnim.SetBool("isWalking", true);
            
            float intermediateSteps = Mathf.Ceil(stepDuration * fps);
            float intermediatePause = stepDuration / intermediateSteps;
            float lerp = 1.0f / intermediateSteps;

            for (var i = 0; i < intermediateSteps; i++)
            {
                float lerpVal = i * lerp;
                gameObject.transform.position = Vector2.Lerp(currentPos, newPos, lerpVal);
                yield return new WaitForSeconds(intermediatePause);
            }

            gameObject.transform.position = newPos;
            currentlyWalking = false;
            playerAnim.SetBool("isWalking", false);
        }
    }

    IEnumerator makeSonarWave()
    {
        playerAnim.SetBool("isShouting", true);
        yield return new WaitForSeconds(0.5f);
        playerAnim.SetBool("isShouting", false);

        currTile = gMScript.getTileFromWorldPos(new Vector2(transform.position.x, transform.position.y));
        currTile.GetComponent<SpriteRenderer>().color = Color.white;

    }

    Vector2 calculateNewPos(Vector2 currentPos, string direction)
    {
        switch (direction)
        {
            case "up":
                return new Vector2(currentPos.x, currentPos.y + stepSize);
            case "down":
                return new Vector2(currentPos.x, currentPos.y - stepSize);
            case "right":
                return new Vector2(currentPos.x + stepSize, currentPos.y);
            case "left":
                return new Vector2(currentPos.x - stepSize, currentPos.y);
            default:
                return currentPos;
        }
    }

    

}
